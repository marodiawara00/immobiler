<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <body>
    <style media="screen">
      .actions {
        list-style: none;
      }
      .actions li{
        margin-bottom: 5px;
        width: 100%;
      }
      .historique{
        margin-top: 20px;
      }
      .historique h2{
        font-family: 'Roboto', sans-serif;
        font-weight: lighter;
        text-transform: uppercase;
        font-size: 20px;
        text-align: center;
      }
    </style>
    <div class="row">
      <div class="card col-4">
         <div class="card-body">
             <img src="{{ $client->piece_identite }}" alt="" width="300" height="400">

         </div>
      </div>
      <div class="col-4">
        <h3>Informations Personnelles</h3>
        <p>Nom: <strong>{{$client->nom}}</strong></p>
        <p>Prenom: <strong>{{$client->prenom}}</strong></p>
        <p>Age: <strong>{{$client->age}}</strong></p>
        <p>Sexe: <strong>{{$client->sexe}}</strong></p>
        <p>Profession: <strong>{{$client->profession}}</strong></p>
        <hr>
        <h3>Coordonnéess</h3>
        <p>Adresse: <strong>{{$client->adresse}}</strong></p>
        <p>Téléphone: <strong>{{$client->telephone}}</strong></p>
        <p>Email: <strong>{{$client->email}}</strong></p>
      </div>
      <div class="col-4">
        <ul  class="col actions">
          <li><a class="btn btn-primary" href="{{route('clients.edit', $client)}}"><i class="fa fa-edit"></i> Modifier</a></li>
          <li>
            <a class="btn btn-default" href="{{route('createLocation', $client->id)}}"><i class="fa fa-house"></i> Nouvelle location</a>
          {{-- <form class="" action="{{route('createLocation', $client->id)}}" method="get">
            @csrf
            <button type="button" name="button">Nouvelle Location</button>
          </form> --}}
          </li>
          <li><a class="btn btn-default" href="{{route('payements.create')}}"><i class="fa fa-coins"></i> Nouveau payement</a></li>
        </ul>

        <form action="{{ route('clients.destroy', $client) }}" method="POST">
            @csrf
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-oblong btn-outline-danger btn-big">Supprimer</a>
        </form>
      </div>

    </div>
    <div class="historique">
      <h2>Historique de location</h2>
      <table class="table ">
        <thead>
          <tr>
            <th>Numéro</th>
            <th>Date début</th>
            <th>Date fin</th>
            <th>Logement</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($client->locations as $location)
            <tr>
              <td>{{$location->id}}</td>
              <td>{{$location->date_debut}}</td>
              <td>{{$location->date_fin}}</td>
              <td>{{$location->logement->code}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>



    <script src="/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.js"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="/dist/js/demo.js"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
    <script src="/plugins/raphael/raphael.min.js"></script>
    <script src="/plugins/jquery-mapael/jquery.mapael.min.js"></script>
    <script src="/plugins/jquery-mapael/maps/usa_states.min.js"></script>
    <!-- ChartJS -->
    <script src="/plugins/chart.js/Chart.min.js"></script>

    <!-- PAGE SCRIPTS -->
    <script src="/dist/js/pages/dashboard2.js"></script>
  </body>
</html>
