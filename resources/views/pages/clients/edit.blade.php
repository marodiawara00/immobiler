<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <body>
<style media="screen">
  .formulaire div{
    margin: auto;
    justify-content: center;
  }
  .button button, .button a{
    margin: auto;
    margin-top: 50px;
    margin-bottom: 50px;
  }
  .card-header h1{
    font-family: 'Roboto', sans-serif;
    font-weight: lighter;
    text-transform: uppercase;
    font-size: 30px;
    text-align: center;
  }
  .subtitle{
    font-family: 'Roboto', sans-serif;
    font-weight: lighter;
    text-transform: uppercase;
    font-size: 20px;
    text-align: center;
  }
</style>

<div class="card">
  <div class="card-header">
    <h1>Modification</h1>
  </div>
  <div class="card-body">
    <form class="" action="{{route('clients.update', $client)}}" method="post">
      @csrf
      <div class="card card-danger">
        <div class="card-body formulaire">
          <br>
          <h2 class="subtitle">Les Informations de {{$client->nom}} {{$client->prenom}}</h2>
          <br>
          <div class="row col-11">
            <div class="col-3">
              <label for="">Nom</label>
              <input name="nom" id="nom" type="text" class="form-control" placeholder="Entrez le nom" value="{{$client->nom}}">
            </div>
            <div class="col-3">
              <label for="">Prénom</label>
              <input name="prenom" id="prenom" type="text" class="form-control" placeholder="Entrez le prenom" value="{{$client->prenom}}">
            </div>
            <div class="col-3">
              <label for="">Téléphone</label>
              <input name="telephone" id="telephone" type="text" class="form-control" placeholder="Entrez le numéro" value="{{$client->telephone}}">
            </div>
          </div>
          <br>
          <div class="row col-11">
            <div class="col-3">
              <label for="">Adresse</label>
              <input name="adresse" id="adresse" type="text" class="form-control" placeholder="Entrez l'adresse" value="{{$client->adresse}}">
            </div>
            <div class="col-3">
              <div class="form-group">
                <label>Sexe</label>
                <select class="custom-select" name="sexe" id="sexe">
                  @if ($client->sexe == "masculin")
                    <option value="{{$client->sexe}}" selected>Masculin</option>
                  @elseif ($client->sexe == "feminin")
                    <option value="{{$client->sexe}}" selected>Féminin</option>
                  @endif
                  <option value="masculin">Masculin</option>
                  <option value="feminin">Feminin</option>
                </select>
              </div>
            </div>
            <div class="col-3">
              <label for="">Age</label>
              <input name="age" type="number" id="age" class="form-control" placeholder="Entrez l'age" value="{{$client->age}}">
            </div>
          </div>
          <br>
          <br>
          <div class="row col-11">
            <div class="col-3">
              <div class="form-group">
                <label for="myfile">Piece d'identité</label>
                <input type="file" id="piece_identite" name="piece_identite" value="{{$client->piece_identite}}">
              </div>
            </div>
            <div class="col-3">
              <label for="">Profession</label>
              <input name="profession" type="text" id="profession" class="form-control" placeholder="Entrez la profession" value="{{$client->profession}}">
            </div>
            <div class="col-3">
              <label for="">Email <i style="color:red">*</i> </label>
              <input name="email" type="email" id="email" class="form-control" placeholder="Entrez l'email" value="{{$client->email ?? ' '}}">
            </div>
          </div>
          <br>
          <div class="row col-11">
            <div class="col-3">
              <div class="form-group">
                <label>Personne à Contacter</label>
                <input name="personne_a_contacter" type="text" id="personne_a_contacter" class="form-control" placeholder="Entrez le nom et prenom" value="{{$client->personne_a_contacter}}">
              </div>
            </div>
            <div class="col-3">
              <label for="">Numero de la personne</label>
              <input name="numero_a_contacter" type="text" id="numero_a_contacter" class="form-control" placeholder="Entrez le numéro" value="{{$client->numero_a_contacter}}">
            </div>
            <div class="col-3">

            </div>
          </div>
          </div>
          </div>
          </div>
          </div>
</body>
</html>
