@extends('layouts.app')
@section('title', 'Nouveau Client')
@section('stylesheet')
  <!-- daterange picker -->
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
@endsection
@section('content')
  <style media="screen">
    .formulaire div{
      margin: auto;
      justify-content: center;
    }
    .button button, .button a{
      margin: auto;
      margin-top: 50px;
      margin-bottom: 50px;
    }
    .card-header h1{
      font-family: 'Roboto', sans-serif;
      font-weight: lighter;
      text-transform: uppercase;
      font-size: 30px;
      text-align: center;
    }
    .subtitle{
      font-family: 'Roboto', sans-serif;
      font-weight: lighter;
      text-transform: uppercase;
      font-size: 20px;
      text-align: center;
    }
  </style>
  <div class="">
    <nav aria-label="breadcrumb" style="height:50px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Acceuil</a></li>
        <li class="breadcrumb-item"><a href="{{route('clients.index')}}">Clients</a> </li>
        <li class="breadcrumb-item active" aria-current="page">Nouveau Client</li>
      </ol>
    </nav>
  </div>
  <div class="card">
    <div class="card-header">
      <h1>Nouveau Client</h1>
    </div>
    <div class="card-body">
      <form class="" action="{{route('clients.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card card-danger">
          <div class="card-body formulaire">
            <br>
            <h2 class="subtitle">Les Informations du Client</h2>
            <br>
            <div class="row col-11">
              <div class="col-3">
                <label for="">Nom</label>
                <input name="nom" id="nom" type="text" class="form-control" placeholder="Entrez le nom">
              </div>
              <div class="col-3">
                <label for="">Prénom</label>
                <input name="prenom" id="prenom" type="text" class="form-control" placeholder="Entrez le prenom">
              </div>
              <div class="col-3">
                <label for="">Téléphone</label>
                <input name="telephone" id="telephone" type="text" class="form-control" placeholder="Entrez le numéro">
              </div>
            </div>
            <br>
            <div class="row col-11">
              <div class="col-3">
                <label for="">Adresse</label>
                <input name="adresse" id="adresse" type="text" class="form-control" placeholder="Entrez l'adresse">
              </div>
              <div class="col-3">
                <div class="form-group">
                  <label>Sexe</label>
                  <select class="custom-select" name="sexe" id="sexe">
                    <option value="null" selected disabled>-- Selectionner --</option>
                    <option value="masculin">Masculin</option>
                    <option value="feminin">Feminin</option>
                  </select>
                </div>
              </div>
              <div class="col-3">
                <label for="">Age</label>
                <input name="age" type="number" id="age" class="form-control" placeholder="Entrez l'age">
              </div>
            </div>
            <br>
            <br>
            <div class="row col-11">
              <div class="col-3">
                <div class="form-group">
                  <label for="myfile">Piece d'identité</label>
                  <input type="file" id="piece_identite" name="piece_identite">
                </div>
              </div>
              <div class="col-3">
                <label for="">Profession</label>
                <input name="profession" type="text" id="profession" class="form-control" placeholder="Entrez la profession">
              </div>
              <div class="col-3">
                <label for="">Email <i style="color:red">*</i> </label>
                <input name="email" type="email" id="email" class="form-control" placeholder="Entrez l'email">
              </div>
            </div>
            <br>
            <div class="row col-11">
              <div class="col-3">
                <div class="form-group">
                  <label>Personne à Contacter</label>
                  <input name="personne_a_contacter" type="text" id="personne_a_contacter" class="form-control" placeholder="Entrez le nom et prenom">
                </div>
              </div>
              <div class="col-3">
                <label for="">Numero de la personne</label>
                <input name="numero_a_contacter" type="text" id="numero_a_contacter" class="form-control" placeholder="Entrez le numéro">
              </div>
              <div class="col-3">

              </div>
            </div>
            <hr>
            <h2 class="subtitle">Informations de la location</h2>
            <br>
            <div class="row col-11">
                <div class="col-3">
                  <label>Type Local</label>
                  <select class="form-control" name="type">
                    <option value="0" selected disabled>--- Selectionner un type ---</option>
                    @foreach ($types as $type)
                      <option value="{{$type->id}}">{{$type->nom}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-3">
                  <label>Local</label>
                  <select class="form-control" name="logement">
                    <option value="0" selected disabled>--- Selectionner un local ---</option>
                    {{-- @foreach ($logements as $logement)
                      <option value="{{$logement->id}}">{{$logement->code}}({{$logement->status}})</option>
                    @endforeach --}}
                  </select>
                </div>
              </div>
              <br>
              <div class="row col-11">

                <div class="col-3">
                  <label>Date de début</label>
                  <input type="date" name="date_debut" class="form-control">
                </div>
                <div class="col-3">
                  <label>Date de fin</label>
                  <input type="date" name="date_fin" class="form-control">
                </div>
              </div>
              <br>
              <div class="row col-11">
                <div class="col-3">
                  <label>Nombre de mois</label>
                  <input type="number" name="number" class="form-control">
                </div>
                <div class="col-3">
                  <label>Caution</label>
                  <input type="text" name="caution" class="form-control" readonly>
                </div>
            </div>
            <br>
            <div class="row col-6 button">
              <a class="btn btn-danger" href="{{route('clients.index')}}">Annuler</a>
              <button class="btn btn-primary" type="submit">Ajouter</button>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
      </form>
    </div>
  </div>
@endsection
@section('script')
  <script src="/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
  <!-- Select2 -->
<script src="/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="/plugins/moment/moment.min.js"></script>
<script src="/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
//   function myFunction() {
//
//   var x = document.getElementById("fname");
//   x.value = x.value.toUpperCase();
// }

$(document).ready(function(){
  $("select[name='type']").change(function () {
            var type_id = $(this).val();
            console.log(type_id);

            if (type_id) {
                $.ajax({
                    url: "{{ route('api.logements') }}?type="+type_id,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        $("select[name='logement']").empty();
                        $("select[name='logement']").append('<option>--- Local ---</option>')
                        $.each(data, function (key, value) {
                            console.log(key);
                            $("select[name='logement']").append('<option value="'+key+'">'+value+'</option>')
                        });
                    }
                });
            } else {
                $("select[name='logement']").empty();
            }
            });

});
</script>
@endsection
