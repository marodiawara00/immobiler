@extends('layouts.app')
@section('title', 'Clients')
@section('stylesheet')
  <link rel="stylesheet" href="/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection
@section('content')
  <style media="screen">
  ol li a{
    font-size: 25px;
  }
  a{
    text-decoration: none;
  }
  .actions{
    display: flex;
    flex-direction: row;
  }
  .actions li{
    list-style: none;
    margin-right: 5px;
  }
  .card-title {
    font-family: 'Roboto', sans-serif;
    font-weight: lighter;
    text-transform: uppercase;
    font-size: 30px;
  }
  .card-header{
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }
  .destroy li{
    list-style: none;
  }
  </style>
  <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" height="1000" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
            <iframe name="content" class="embed-responsive-item" allowfullscreen></iframe>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Fermer</button>
        </div>

      </div>
    </div>
  </div>
  <div class="">
    <nav aria-label="breadcrumb" style="height:50px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Acceuil</a></li>
        <li class="breadcrumb-item active" aria-current="page">Clients</li>
      </ol>
    </nav>
  </div>
  <div class="card">
    <div class="card-header row">
      <h3 class="card-title">Clients</h3>
      <a class="btn  btn-primary" href="{{route('clients.create')}}">Nouveau Client</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Numéro</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Age</th>
            <th>Téléphone</th>
            <th>Pièce d'identité</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($clients as $client)
            <tr>
              <td>{{$client->id}}</td>
              <td>{{$client->nom}}</td>
              <td>{{$client->prenom}}</td>
              <td>{{$client->age}}</td>
              <td>{{$client->telephone}}</td>
              <td>{{$client->piece_identite}}</td>
              <td>
                <ul class="row actions">
                  <li data-toggle="modal" data-target="#exampleModal" ><a class="btn btn-oblong btn-outline-primary btn-big" href="{{route('clients.show', $client)}}" target="content"> <i class="fa fa-eye"></i> Voir</a></li>
                </ul>
              </td>
              <td>
                <form action="{{ route('clients.destroy', $client) }}" method="POST">
                    @csrf
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-oblong btn-outline-danger btn-big"> <i class="fa fa-trash"></i> Supprimer</a>
                </form>
              </td>

            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>Numéro</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Age</th>
            <th>Téléphone</th>
            <th>Pièce d'identité</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection
@section('script')
  <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="/dist/js/demo.js"></script>
  <!-- page script -->
  <script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
