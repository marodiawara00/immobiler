@extends('layouts.app')
@section('title', 'Nouveau Local')
@section('stylesheet')
  <!-- daterange picker -->
  <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
@endsection
@section('content')
  <style media="screen">
  .formulaire div{
    margin: auto;
    justify-content: center;
  }
  .button button{
    margin: auto;
    margin-top: 50px;
    margin-bottom: 50px;
  }
  .card-header h1{
    font-family: 'Roboto', sans-serif;
    font-weight: lighter;
    text-transform: uppercase;
    font-size: 30px;
    text-align: center;
  }
  </style>
  <div class="card">
    <div class="card-header">
      <h1>Nouveau Local</h1>
    </div>
    <div class="card-body">
      <form class="" action="{{route('logements.store')}}" method="post">
        @csrf
        <div class="card card-danger">
          <div class="card-body formulaire">
            <div class="row col-11">
              <div class="col-3">
                <label for="">Commune</label>
                <select class="form-control" name="commune">
                  <option value="" selected>-- Selectionnez la commune --</option>
                  @foreach($communes as $commune)
                    <option value="{{$commune->id}}">{{$commune->commune}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-3">
                <label for="">Quartier</label>
                <select class="form-control" name="quartier">
                </select>
              </div>
              <div class="col-3">
                <label for="">Prix</label>
                <input name="prix" type="number" id="prix" class="form-control" placeholder="Entrez le prix" required>
              </div>
              <div class="col-3">
                <label for="">Description</label>
                <textarea name="description" rows="3" cols="80" class="form-control" placeholder="Entrez la description" required></textarea>
                {{-- <input name="description" type="text" id="description" class="form-control" placeholder="Entrez la description"> --}}
              </div>
            </div>
            <br>
            <div class="row col-11">



              <div class="col-3">
                <label for="">Type de logement</label>
                <select class="form-control" name="type" required>
                  <option value="0" disabled selected>--- Selectionner un type --</option>
                  @foreach ($types as $type)
                    <option value="{{$type->id}}">{{$type->nom}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-3">
                <label for="">Statut</label>
                <select class="form-control" name="status" required>
                  <option value="disponible">Disponible</option>
                  <option value="occupé">Occupé</option>
                </select>
              </div>
              </div>
              <br>
              <div class="col-8">
                <div class="input-group control-group increment" >
                  <label for="">Photos du local</label>
                  <input type="file" name="pictures[]" class="form-control">
                  <div class="input-group-btn">
                    <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Ajouter</button>
                  </div>
                </div>
                <div class="clone hide" hidden>
                  <div class="control-group input-group" style="margin-top:10px">
                    <input type="file" name="pictures[]" class="form-control">
                    <div class="input-group-btn">
                      <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Supprimer</button>
                    </div>
                  </div>
                </div>

              </div>
              <br>
              <div class="row col-6 button">
                <button class="btn btn-danger" type="reset" name="button" value="Annuler">Annuler</button>
                <button class="btn btn-primary" type="submit">Ajouter</button>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </form>
      </div>
    </div>
  @endsection
  @section('script')
    <script src="/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <!-- Select2 -->
    <script src="/plugins/select2/js/select2.full.min.js"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
    <!-- InputMask -->
    <script src="/plugins/moment/moment.min.js"></script>
    <script src="/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- date-range-picker -->
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <script src="/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <script>
    $(document).ready(function () {
      $("select[name='commune']").change(function () {
             var commune_id = $(this).val();
             if (commune_id) {
                 $.ajax({
                     url: "{{ route('api.quartiers') }}?commune="+commune_id,
                     type: "GET",
                     dataType: "json",
                     success: function(data) {
                         console.log(data);
                         $("select[name='quartier']").empty();
                         $("select[name='quartier']").append('<option>--- Quartier ---</option>')
                         $.each(data, function (key, value) {
                             $("select[name='quartier']").append('<option value="'+key+'">'+value+'</option>')
                         });
                     }
                 });
             } else {
                 $("select[name='quartier']").empty();
             }
         });



    $(".btn-success").click(function(){
              var html = $(".clone").html();
              $(".increment").after(html);
          });

          $("body").on("click",".btn-danger",function(){
              $(this).parents(".control-group").remove();
          });
});

    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })

      //Datemask dd/mm/yyyy
      $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
      //Datemask2 mm/dd/yyyy
      $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
      //Money Euro
      $('[data-mask]').inputmask()

      //Date range picker
      $('#reservation').daterangepicker()
      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
          format: 'MM/DD/YYYY hh:mm A'
        }
      })
      //Date range as a button
      $('#daterange-btn').daterangepicker(
        {
          ranges   : {
            'Today'       : [moment(), moment()],
            'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month'  : [moment().startOf('month'), moment().endOf('month')],
            'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate  : moment()
        },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        }
      )

      //Timepicker
      $('#timepicker').datetimepicker({
        format: 'LT'
      })

      //Bootstrap Duallistbox
      $('.duallistbox').bootstrapDualListbox()

      //Colorpicker
      $('.my-colorpicker1').colorpicker()
      //color picker with addon
      $('.my-colorpicker2').colorpicker()

      $('.my-colorpicker2').on('colorpickerChange', function(event) {
        $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
      });

      $("input[data-bootstrap-switch]").each(function(){
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
      });

    })
    </script>
  @endsection
