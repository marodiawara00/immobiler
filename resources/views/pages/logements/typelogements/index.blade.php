@extends('layouts.app')
@section('title', 'TypeLogements')
@section('stylesheet')
  <link rel="stylesheet" href="/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection
@section('content')
  <style media="screen">
  ol li a{
    font-size: 25px;
  }
  .entete{
    justify-content: space-between;
  }
  .actions{
    display: flex;
    flex-direction: row;
  }
  .actions li{
    list-style: none;
    margin-right: 5px;
  }
  .card-title {
    font-family: 'Roboto', sans-serif;
    font-weight: lighter;
    text-transform: uppercase;
    font-size: 30px;
  }
  </style>
  <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" height="1000" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
            <iframe name="content" class="embed-responsive-item" allowfullscreen></iframe>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Fermer</button>
        </div>

      </div>
    </div>
  </div>
  <div class="">
    <nav aria-label="breadcrumb" style="height:50px;">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Acceuil</a></li>
        <li class="breadcrumb-item active" aria-current="page">Type de logements</li>
      </ol>
    </nav>
  </div>
  <div class="card">
    <div class="row entete card-header">
      <h3 class="card-title">Type de logements</h3>
      <a class="btn  btn-primary" href="{{route('typelogements.create')}}">Nouveau Type de logement</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Numéro</th>
            <th>Nom</th>
            <th>Code</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($types as $type)
            <tr>
              <td>{{$type->id}}</td>
              <td>{{$type->nom}}</td>
              <td>{{$type->code}}</td>
              <td data-toggle="modal" data-target="#exampleModal">
                <ul  class="row actions">
                  <li ><a class="btn btn-default" href="{{route('typelogements.show', $type)}}" target="content"><i class="fa fa-eye"></i> Voir</a></li>
                  <li><a class="btn btn-primary" href="{{route('typelogements.edit', $type)}}"><i class="fa fa-edit"></i> Modifier</a></li>
                  <li><a class="btn btn-danger" href="{{route('typelogements.destroy', $type)}}"><i class="fa fa-trash"></i> Supprimer</a></li>
                </ul>
              </td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>Numéro</th>
            <th>Nom</th>
            <th>Code</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection
@section('script')
  <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="/dist/js/demo.js"></script>
  <!-- page script -->
  <script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
