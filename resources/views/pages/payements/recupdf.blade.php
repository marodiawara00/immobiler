<!doctype html>

    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title> RECU - {{$payement->id}} </title>
    </head>
    <body>


      <style>
      .entete{
        display: flex;
        flex-direction: row;
        justify-content: center;
      }
      .image{
        float: right;
      }
          /* reset */

          /***/
          /*{*/
          /*border: 0;*/
          /*box-sizing: content-box;*/
          /*color: inherit;*/
          /*font-family: inherit;*/
          /*font-size: inherit;*/
          /*font-style: inherit;*/
          /*font-weight: inherit;*/
          /*line-height: inherit;*/
          /*list-style: none;*/
          /*margin: 0;*/
          /*padding: 0;*/
          /*text-decoration: none;*/
          /*vertical-align: top;*/
          /*}*/

          /* content editable */

          *[contenteditable] { border-radius: 0.25em; min-width: 1em; outline: 0; }

          *[contenteditable] { cursor: pointer; }

          *[contenteditable]:hover, *[contenteditable]:focus, td:hover *[contenteditable], td:focus *[contenteditable], img.hover { background: #DEF; box-shadow: 0 0 1em 0.5em #DEF; }

          span[contenteditable] { display: inline-block; }

          /* heading */

          h1 { font: bold 100% sans-serif; letter-spacing: 0.5em; text-align: center; text-transform: uppercase; }

          /* table */

          table { font-size: 75%; table-layout: fixed; width: 100%; }
          table { border-collapse: separate; border-spacing: 2px; }
          th, td { border-width: 1px; padding: 0.5em; position: relative; text-align: left; }
          th, td { border-radius: 0.25em; border-style: solid; }
          th { background: #EEE; border-color: #BBB; }
          td { border-color: #DDD; }

          /* page */

          /*html { font: 16px/1 'Open Sans', sans-serif; overflow: auto; padding: 0.5in; }*/
          /*html { background: #999; cursor: default; }*/

          /*body { box-sizing: border-box; height: 11in; margin: 0 auto; overflow: hidden; padding: 0.5in; width: 8.5in; }*/
          /*body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }*/

          /* header */

          header { margin: 0 0 3em; }
          header:after { clear: both; content: ""; display: table; }

          header h1 { background: #000; border-radius: 0.25em; color: #FFF; margin: 0 0 1em; padding: 0.5em 0; }
          header address { float: left; font-size: 75%; font-style: normal; line-height: 1.25; margin: 0 1em 1em 0; }
          header address p { margin: 0 0 0.25em; }
          header span, header img { display: block; float: right; }
          header span { margin: 0 0 1em 1em; max-height: 25%; max-width: 60%; position: relative; }
          header img { max-height: 100%; max-width: 100%; }
          header input { cursor: pointer; -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; height: 100%; left: 0; opacity: 0; position: absolute; top: 0; width: 100%; }

          /* article */

          article, article address, table.meta, table.inventory { margin: 0 0 3em; }
          article:after { clear: both; content: ""; display: table; }
          article h1 { clip: rect(0 0 0 0); position: absolute; }

          article address { float: left; font-size: 125%; font-weight: bold; }

          /* table meta & balance */

          table.meta, table.balance { float: right; width: 36%; }
          table.meta:after, table.balance:after { clear: both; content: ""; display: table; }

          /* table meta */

          table.meta th { width: 40%; }
          table.meta td { width: 60%; }

          /* table items */

          table.inventory { clear: both; width: 100%; }
          table.inventory th { font-weight: bold; text-align: center; }

          table.inventory td:nth-child(1) { width: 26%; }
          table.inventory td:nth-child(2) { width: 38%; }
          table.inventory td:nth-child(3) { text-align: right; width: 12%; }
          table.inventory td:nth-child(4) { text-align: right; width: 12%; }
          table.inventory td:nth-child(5) { text-align: right; width: 12%; }

          /* table balance */

          table.balance th, table.balance td { width: 50%; }
          table.balance td { text-align: right; }

          /* aside */

          aside h1 { border: none; border-width: 0 0 1px; margin: 0 0 1em; }
          aside h1 { border-color: #999; border-bottom-style: solid; }

          /* javascript */

          .add, .cut
          {
              border-width: 1px;
              display: block;
              font-size: .8rem;
              padding: 0.25em 0.5em;
              float: left;
              text-align: center;
              width: 0.6em;
          }

          .add, .cut
          {
              background: #9AF;
              box-shadow: 0 1px 2px rgba(0,0,0,0.2);
              background-image: -moz-linear-gradient(#00ADEE 5%, #0078A5 100%);
              background-image: -webkit-linear-gradient(#00ADEE 5%, #0078A5 100%);
              border-radius: 0.5em;
              border-color: #0076A3;
              color: #FFF;
              cursor: pointer;
              font-weight: bold;
              text-shadow: 0 -1px 2px rgba(0,0,0,0.333);
          }

          .add { margin: -2.5em 0 0; }

          .add:hover { background: #00ADEE; }

          .cut { opacity: 0; position: absolute; top: 0; left: -1.5em; }
          .cut { -webkit-transition: opacity 100ms ease-in; }

          tr:hover .cut { opacity: 1; }

      </style>
    <br>
      <div class="entete row">
        <div class="">
          <h2 color="blue">House RENT</h2>
          <p><i>Adresse</i></p>
          <p><i>Rue, Porte</i></p>
          <p><i>Téléphone</i></p>
          <p><i>Email</i></p>
        </div>
        <div class="image">
          <img src="/dist/img/house.png" alt="" >
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col">
          <h3>Recu pour:</h3>
          <p>{{$payement->location->client->nom}} {{$payement->location->client->prenom}}</p>
          <p>{{$payement->location->client->adresse}}</p>
          <p>{{$payement->location->client->telephone}}</p>
        </div>
        <div class="col">
          <p>Date du payement: {{$payement->date}}</p>
          <p>Date du recu: {{ date('Y-m-d H:i:s') }}</p>
          <p>Recu n° {{$payement->id}}</p>
        </div>
      </div>
      <div class="col">
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Adresse du local</th>
              <th>Montant</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$payement->location->logement->adresse}}</td>
              <td>{{$payement->montant}} FCFA</td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>
      <br>
      <br>
      <p>Signature</p>
    </body>
    </html>
{{-- <!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title> RECU - {{$payement->id}} </title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
  <body>
    <div class="row">
      <div class="col">
        <h2 color="blue">House RENT</h2>
        <p><i>Adresse</i></p>
        <p><i>Rue, Porte</i></p>
        <p><i>Téléphone</i></p>
        <p><i>Email</i></p>
      </div>
      <div class="">
        <img src="/dist/img/house.png" alt="" class="img-size-50 mr-3 img-circle">
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col">
        <h3>Recu pour:</h3>
        <p>{{$payement->location->client->nom}} {{$payement->location->client->prenom}}</p>
        <p>{{$payement->location->client->adresse}}</p>
        <p>{{$payement->location->client->telephone}}</p>
      </div>
      <div class="col">
        <p>Date du payement: {{$payement->date}}</p>
        <p>Date du recu: {{ date('Y-m-d H:i:s') }}</p>
        <p>Recu n° {{$payement->id}}</p>
      </div>
    </div>
    <div class="">
      <table>
        <thead>
          <th>Adresse du local</th>
          <th>Montant Payé</th>
        </thead>
        <tbody>
          <tr>
            <td>{{$payement->location->logement->adresse}}</td>
            <td>{{$payement->montant}} FCFA</td>
          </tr>
        </tbody>
      </table>
    </div>
    <br>
    <br>
    <br>
    <p>Signature</p>
  </body>
</html> --}}
