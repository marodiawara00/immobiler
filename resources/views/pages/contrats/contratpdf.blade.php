<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Contrat de Location</title>
  </head>
  <body>
    <div class="">
      <h1>Contrat de Location</h1>
    </div>
    <br>
    <div class="">
      <h3>Désignation des parties</h3>
    </div>
    <div class="">
      <p>le present contrat est conclu entre les soussignés:</p>
      <h4>1. Le Bailleur</h4>
      <p>Monsieur Diawara M'Paly, né le 12/12/12 à ......., demeurant à Bacodjicoroni ACI</p>
      <p>Désigné ci-après, <strong>Le bailleur</strong> </p>
      <p>Et, d'autre part,</p>
      <br>
      <h4>2. Le Locataire</h4>
      <p> @if ($contrat->location->client->sexe == "feminin")
        Madame
      @elseif ($contrat->location->client->sexe == "masculin")
        Monsieur
      @endif {{$contrat->location->client->nom}} {{$contrat->location->client->prenom}} ,  demeurant à {{$contrat->location->client->adresse}} </p>
      <p>Désigné ci-après, <strong>Le locataire</strong> </p>
      <br>
      <p>Le Bailleur et le Locataire étant ci-après désignés, ensemble, les <strong>"Parties"</strong>.</p>
    </div>
    <div class="">
      <h3>Il a été convenu ce qui suit</h3>
      <p>Par les presentes, le bailleur consent un bail d'habitation portant sur le bien désigné ci-apres, ao locataire qui declare y installer sa residence principales et les accepte.</p>
      <br>
      <h3>Acticles 1 - Objet du contrat</h3>
      <p>Le present contrat a pour objet la location d'un appartement ainsi déterminé:</p>
      <br>
      <h4>1.1 Consistance du logement </h4>
      <h5>1.1.1 Adresse du logement </h5>
      <p>Le logement est situé {{$contrat->location->logement->adresse}} , Bamako</p>
      <p>Le logement est situé à l'etage n° ......, appartement n° .......</p>
      <br>
      <h5>1.1.2 Caractéristiques du logement</h5>
      <ol>
        <li>Nombre de pièces principales:  </li>
        <li>Types de pieces principales: chambre, salon, toilette</li>
      </ol>
      <br>
      <h4>1.2 Destination des locaux</h4>
      @if ($contrat->location->type == "1")
        <p>Les locaux sont à usage exclusif de magasin, le locataire y installant son magasin</p>
      @elseif ($contrat->location->type == "2")
        <p>Les locaux sont à usage exclusif d'habitation', le locataire y installant sa resisdence principal</p>
      @endif
      <br>
      <h3>Article 2 - Date de prise d'effet et durée du contrat</h3>
      <h4>2.1 Date de prise d'effet du contrat</h4>
      <p>Le contrat prent effet à compter du {{$contrat->location->date_debut}} </p>
      <br>
      <h4>2.2 Durée du contrat</h4>
      <p>Le bail est d'une durée de {{$contrat->duree}} .</p>
      <br>
      <h3>Article 3 - Conditions Financières</h3>
      <h4>Fixation du loyer</h4>
      <h5>Fixation du loyer initial</h5>
      <p>Le montant du loyer mensuel est fixée à {{$contrat->location->logement->prix}} F CFA hors charges .</p>
      <p>Le montant du loyer sera payable au bureau du Bailleur le ... de chaque mois.</p>
      <p>Le preneur verse au jour de la signature du present bail au bailleur une caution de ...... F CFA.</p>
      <br>
      <h3>Article 4- Etat des lieux</h3>
      <p>Le Locataire est tenu responsable de l'etat, de l'entretien des lieux durand toute la durée de la location. </p>
      <br>
      <h3>Article 6-COnditions générales </h3>
      <h4>6.1 Obligations sur le bailler</h4>
      <p>Le bailleur est tenu des oligations princpales suivantes:</p>
      <ol>
        <li>Delivrer le logement en bon état d'usage et de réparation, ainsi que les équipement mentionnés au contrat </li>
        <li>Remettre gratuitement et mensuellement une quitance de loyer au Locataire. Délivrer un reçu lorsque le locataire fait un paiement partiel.</li>
        <li>Avertir le locataire trois (3) en avance avec un preavis en cas de besoin de ces locaux.</li>
      </ol>
      <br>
      <h4>6.2 Obligations sur le locataire</h4>
      <p>Le locataire est tenu des oligations princpales suivantes:</p>
      <ol>
        <li>Payer le loyer et les charges récuperables au terme convenu.</li>
        <li>Prendre en charge l'entretion courant du logement et des equipements mentionnés.</li>
        <li>Informer immediatement le bailleur de tout sinistre et degradation se produisant dans les locaux loués, même s'il n'en resulte aucun dommage apparent. Cette infromation n'engage pas la responsabilité du locataire lorsque les degats ne sont sont pas de son fait personnel.</li>
        <li>Ne pas transformer sans accord écrit du bailleur les locaux loués et leurs equipements. Dans le cas contraire le bailleur pourra soit demander la remise en état aux frais du locataire des locaux et équipements, soit la conservation des transformations sans que le locatire soit indemnisé. </li>
        <li>Laisser exécuter dans les locaux loués chaque fois que cela sera rendu nécessaire pour les reparations, la sécurité ou la salubrité de l'immeuble. Ces visites devront être effectuées, sauf urgence, les jours ouvrables après que le locataire en ait été préalablement averti. </li>

      </ol>
      <br><br>
      <hr>
      <p>Fait à Bamako en 2 exemplaires originaux,</p>
      <p>le {{$contrat->created_at}}</p>
      <br> <br>

      <strong>Le Bailleur</strong>
      <strong>Le Locataire</strong>

    </div>
  </body>
</html>
