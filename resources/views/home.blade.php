@extends('layouts.app')
@section('title', 'Acceuil')
@section('content')
  <style media="screen">
    .tables{
      display: flex;
      justify-content: space-around;
      flex-direction: row;

    }
    .titre{
      display: flex;
      justify-content: space-between;
      flex-direction: row;
    }
    .titre a{
      margin: auto;
      margin-right: 0px;
    }
    .titre h2{
      font-family: 'Roboto', sans-serif;
      font-weight: lighter;
      text-transform: uppercase;
      font-size: 20px;
    }
  </style>
  <div class="row">
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box">
        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Nombre de clients</span>
          <span class="info-box-number">
            {{$clients}}
            <small>clients</small>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-building"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Locations en cours</span>
          <span class="info-box-number">{{$locations}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix hidden-md-up"></div>

    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-home"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Nombre locaux</span>
          <span class="info-box-number">{{$logements}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-coins"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Payements</span>
          <span class="info-box-number">{{$payements}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <br> <br>
  <div class="tables">
    <div class="card ">
      <div class="titre card-header">
        <h2>Dernières locations</h2>
        <a href="{{route('locations.index')}}">Toutes les locations</a>
      </div>
      <div class="card-body table-responsive p-0 ">
        <table class="table table-striped table-valign-middle">
          <thead>
            <tr>
              <th>N°</th>
              <th>Client</th>
              <th>Logement</th>
              <th>Date de début</th>
              <th>Date de fin</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($locations_liste as $location)
              <tr>
                <td>{{$location->id}}</td>
                <td>{{$location->client->nom}} {{$location->client->prenom}}</td>
                <td>{{$location->logement->code}}</td>
                <td>{{$location->date_debut}}</td>
                <td>{{$location->date_fin}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="card">
      <div class="titre card-header">
        <h2>Derniers payements</h2>
        <a href="{{route('payements.index')}}">Tous les payements</a>
      </div>
      <div class="card-body table-responsive p-0">
        <table class="table table-striped table-valign-middle">
          <thead>
            <tr>
              <th>N°</th>
              <th>Client</th>
              <th>Montant</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($payements_liste as $payement)
              <tr>
                <td>{{$payement->id}}</td>
                <td>{{$payement->location->client->nom}} {{$payement->location->client->prenom}}</td>
                <td>{{$payement->montant}} F CFA</td>
                <td>{{$payement->date}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>


@endsection
