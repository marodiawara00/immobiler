<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logements', function (Blueprint $table) {
          $table->id();
          $table->string('commune');
          $table->string('quartier');
          $table->double('prix', 10, 2);
          $table->string('status');
          $table->text('description');
          $table->string('code');
          $table->text('pictures')->nullable();
          $table->unsignedBigInteger('type');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logements');
    }
}
