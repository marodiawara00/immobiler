<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuartiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quartiers', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('quartier');
          $table->bigInteger('village_id')->unsigned()->nullable();
          $table->bigInteger('commune_id')->unsigned()->nullable();
          $table->bigInteger('cercle_id')->unsigned()->nullable();
          $table->bigInteger('region_id')->unsigned()->nullable();
          $table->bigInteger('country_id')->unsigned()->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quartiers');
    }
}
