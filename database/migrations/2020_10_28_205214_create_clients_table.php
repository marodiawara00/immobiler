<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
          $table->id();
          $table->string('nom');
          $table->string('prenom');
          $table->string('adresse');
          $table->string('age');
          $table->string('telephone');
          $table->string('sexe');
          $table->string('piece_identite');
          $table->string('email');
          $table->string('profession');
          $table->string('personne_a_contacter');
          $table->string('numero_a_contacter');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
