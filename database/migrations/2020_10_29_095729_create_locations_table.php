<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
          $table->id();
          $table->dateTime('date_debut');
          $table->dateTime('date_fin');
          $table->unsignedBigInteger('client_id');
          $table->foreign('client_id')->references('id')->on('clients');
          $table->unsignedBigInteger('logement_id');
          $table->string('caution')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
