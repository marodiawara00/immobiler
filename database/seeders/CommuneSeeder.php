<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Commune;

class CommuneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {

         // Bamako
         Commune::firstOrCreate(['commune' => 'Commune I']);
         Commune::firstOrCreate(['commune' => 'Commune II']);
         Commune::firstOrCreate(['commune' => 'Commune III']);
         Commune::firstOrCreate(['commune' => 'Commune IV']);
         Commune::firstOrCreate(['commune' => 'Commune V']);
         Commune::firstOrCreate(['commune' => 'Commune VI']);

     }
 }
