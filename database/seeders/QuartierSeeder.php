<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Quartier;
use App\Models\Commune;

class QuartierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {

         // Commune I
         Quartier::firstOrCreate(['quartier' => 'Banconi', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Boulkassoumbougou', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Djélibougou', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Doumanzana', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sikoroni', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Fadjiguila', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Korofina Nord', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Korofina Sud', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sotuba', 'commune_id' => Commune::where('commune', 'Commune I')->first()->id]);

         // Commune II
         Quartier::firstOrCreate(['quartier' => 'Niarela', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bagadadji', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Médina Coura', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bozola', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Missira', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Hippodrome', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Quinzambougou', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bakaribougou', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Zone Industrielle', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'TSF', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bougouba', 'commune_id' => Commune::where('commune', 'Commune II')->first()->id]);

         // Commune III
         Quartier::firstOrCreate(['quartier' => 'Sanankoro Farako', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sirakoro Dounfing', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sokonafing', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Quartier du fleuve', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Badialan I', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Badialan III', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Badialan III', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bamako Coura', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bolibana', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Darsalam', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Dravela', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Niomirambougou', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Ntomikorobougou', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Ouolofobougou', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Koulouba', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Same', 'commune_id' => Commune::where('commune', 'Commune III')->first()->id]);

         // Commune IV
         Quartier::firstOrCreate(['quartier' => 'Taliko', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Lassa', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sibiribougou', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Djikoroni para', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sébénikoro', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Hamdallaye', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Hamdallaye ACI 2000', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Lafiabougou', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Kalabambougou', 'commune_id' => Commune::where('commune', 'Commune IV')->first()->id]);

         // Commune V
         Quartier::firstOrCreate(['quartier' => 'Badalabougou', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Badalabougou Sema I', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Quartier Mali', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Torokorobougou', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bacodjicoroni', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bacodjicoroni ACI', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Bacodjicoroni GOLF', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sabalibougou', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Daoudabougou', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Kalaban Coura', 'commune_id' => Commune::where('commune', 'Commune V')->first()->id]);

         // Commune VI
         Quartier::firstOrCreate(['quartier' => 'Sogoniko', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Magnambougou', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sokorodji', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Missabougou', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Faladiè', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Niamakoro', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Djandjiguila', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Banakabougou', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Sénou', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
         Quartier::firstOrCreate(['quartier' => 'Yirimadio', 'commune_id' => Commune::where('commune', 'Commune VI')->first()->id]);
     }
 }
