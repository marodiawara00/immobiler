<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call([
        CommuneSeeder::class,
        QuartierSeeder::class,
      ]
    );
        // \App\Models\User::factory(10)->create();
        \App\Models\User::create(['nom'=>'Diawara','prenom'=>'Maro','username'=>'maro123','email'=>'marodiawara00@gmail.com','password'=>Hash::make('15811maro')]);
    }
}
