<?php

namespace Database\Factories;

use App\Models\Payement;
use Illuminate\Database\Eloquent\Factories\Factory;

class PayementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
