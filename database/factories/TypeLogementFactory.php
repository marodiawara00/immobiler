<?php

namespace Database\Factories;

use App\Models\TypeLogement;
use Illuminate\Database\Eloquent\Factories\Factory;

class TypeLogementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TypeLogement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
