<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Logement;
use App\Models\Commune;
use App\Models\Quartier;
use App\Models\TypeLogement;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/logements', function()
// {
//   $logements = Logement::all();
//   return response()->json($logements);
// })->name('api.logements');

Route::get('/logements', function (Request $request) {
    $logements = Logement::where('type', $request->type)->pluck('code', 'id');

    return response()->json($logements);
})->name('api.logements');

Route::get('/logements/{id}', function($id)
{
  $type = TypeLogement::find($id);
  $logement = Logement::where('type', $type->id)->pluck('code', 'id');
  // $ids = $logement->id;
  // $logement1 = Logement::find($ids);
  return response()->json($logement);
})->name('api.logement');

Route::get('/regions', function () {
    return Region::orderBy('region')->get();
})->name('api.regions');

Route::get('/cercles', function (Request $request) {
    $cercles = Cercle::where('region_id', $request->region)->pluck('cercle', 'id');

    return response()->json($cercles);
})->name('api.cercles');

Route::get('/communes', function () {

    return Commune::orderBy('commune')->get();
})->name('api.communes');

Route::get('/quartiers', function (Request $request) {
    $quartiers = Quartier::where('commune_id', $request->commune)->pluck('quartier', 'id');

    return response()->json($quartiers);
})->name('api.quartiers');
