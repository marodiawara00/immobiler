<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return redirect()->route('home');
});

Auth::routes();
Route::get('/logout', function () {
    Auth::logout();

    return redirect()->route('login');
})->name('logout');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/clients', 'ClientController');
Route::resource('/logements', 'LogementController');
Route::resource('/typelogements', 'TypeLogementController');
Route::resource('/locations', 'LocationController');
Route::resource('/contrats', 'ContratController');
Route::resource('/configurations', 'ConfigurationController');
Route::resource('/payements', 'PayementController');
Route::resource('/users', 'UserController');
Route::get('/recu/{payement}', 'PayementController@recu')->name('recupdf');
Route::get('/contrat/{location}', 'ContratController@contrat')->name('contratpdf');
Route::get('/locations/{client}', 'ClientController@createLocation')->name('createLocation');
