<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\TypeLogement;
use App\Models\Client;
use Illuminate\Http\Request;
use PDF;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();
        return view('pages.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $types = TypeLogement::all();
      $clients = Client::all();
        return view('pages.locations.create', compact('types','clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'date_debut'    => 'required|date',
          'date_fin'    => 'required|date',
          'client_id'    => 'required',
          'logement_id'    => 'required',
        ]);

        $location = Location::create([
          'date_debut'     =>$request->date_debut,
          'date_fin'     =>$request->date_fin,
          'client_id'     =>$request->client_id,
          'logement_id'     =>$request->logement_id,
        ]);

        return redirect()->route('locations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        return view('pages.locations.show', compact('location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
      return view('pages.locations.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
        return redirect()->route('locations.index') ;
    }

    
}
