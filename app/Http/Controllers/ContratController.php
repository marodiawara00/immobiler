<?php

namespace App\Http\Controllers;

use App\Models\Contrat;
use App\Models\Location;
use Illuminate\Http\Request;
use PDF;

class ContratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contrats = Contrat::all();
        return view('pages.contrats.index', compact('contrats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.contrats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'date'   => 'required',
          'duree'   => 'required|date',
          'location_id'   => 'required',
        ]);

        $contrat = Contrat::create([
          'date'      => $request->date,
          'duree'      => $request->duree,
          'location_id'      => $request->location_id,
        ]);

        return redirect()->route('contrats.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contrat  $contrat
     * @return \Illuminate\Http\Response
     */
    public function show(Contrat $contrat)
    {
        return view('pages.contrats.show', compact('contrat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contrat  $contrat
     * @return \Illuminate\Http\Response
     */
    public function edit(Contrat $contrat)
    {
      return view('pages.contrats.edit', compact('contrat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contrat  $contrat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contrat $contrat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contrat  $contrat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contrat $contrat)
    {
        $contrat->delete();
        return redirect()->route('contrats.index');
    }

    public function contrat(Location $location)
    {
      $contrat = Contrat::where('location_id', $location->id)->first();
      // dd($contrat->location->id);
      view()->share('contrat',$contrat);
      $pdf = PDF::loadView('pages.contrats.contratpdf', compact('contrat'));

      // download PDF file with download method
      return $pdf->download('pdf_file.pdf');
    }
}
