<?php

namespace App\Http\Controllers;

use App\Models\TypeLogement;
use Illuminate\Http\Request;

class TypeLogementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = TypeLogement::all();
        return view('pages.logements.typelogements.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pages.logements.typelogements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'nom'   =>'required|string',
        ]);

        $type = new TypeLogement();
        $type->nom   = $request->nom;
        $code = substr($request->nom, 0, 4).$type->id;
        $type->code  = $code;
        $type->save();

        return redirect()->route('typelogements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeLogement  $typeLogement
     * @return \Illuminate\Http\Response
     */
    public function show(TypeLogement $typeLogement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeLogement  $typeLogement
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeLogement $typeLogement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeLogement  $typeLogement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeLogement $typeLogement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeLogement  $typeLogement
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypeLogement $typeLogement)
    {
        //
    }
}
