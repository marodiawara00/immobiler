<?php

namespace App\Http\Controllers;

use App\Models\Logement;
use App\Models\Commune;
use App\Models\TypeLogement;
use Illuminate\Http\Request;

class LogementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // dd("bonjour");
        $logements = Logement::all();
        return view('pages.logements.index', compact('logements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $communes = Commune::all();
        $types = TypeLogement::all();
        return view('pages.logements.create', compact('types', 'communes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request->commune);
        $type = TypeLogement::find($request->type);

        $logement = Logement::firstOrCreate([
          'commune'       =>$request->commune,
          'quartier'      =>$request->quartier,
          'prix'          =>$request->prix,
          'status'        =>$request->status,
          'description'   =>$request->description,
          'code'          =>$type->nom."00".mt_rand(1,100),
          'type'          =>$type->id,
        ]);

        // if($request->hasfile('pictures'))
        //  {
        //
        //     foreach($request->file('pictures') as $picture)
        //     {
        //       $filename = time() . '-piece-identite-.' . $request->piece_identite->extension();
        //       $filename = '/uploads/avatars/' . $filename;
        //       // dd($filename);
        //       $location = public_path($filename);
        //       Image::make($avatar)->save($location);
        //       $client->piece_identite = $filename;
        //       $client->save();
        //     }
        //  }


        return redirect()->route('logements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Logement  $logement
     * @return \Illuminate\Http\Response
     */
    public function show(Logement $logement)
    {
      return view('pages.logements.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Logement  $logement
     * @return \Illuminate\Http\Response
     */
    public function edit(Logement $logement)
    {
      return view('pages.logements.create');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Logement  $logement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Logement $logement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Logement  $logement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Logement $logement)
    {
        $logement->delete();
        return redirect()->route('logements.index');
    }
}
