<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Logement;
use App\Models\Location;
use App\Models\Payement;
use Illuminate\Database\Eloquent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $locations_liste =  Location::latest()->take(5)->get();
      $payements_liste = Payement::latest()->take(5)->get();
      $clients = Client::count();
      $logements = Logement::count();
      $locations = Location::count();
      $payements = Payement::count();
        return view('home', compact('clients','logements','locations','payements','locations_liste','payements_liste'));
    }
}
