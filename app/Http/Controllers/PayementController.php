<?php

namespace App\Http\Controllers;

use App\Models\Payement;
use Illuminate\Http\Request;
Use PDF;
Use \Carbon\Carbon;

class PayementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payements = Payement::all();
        return view('pages.payements.index', compact('payements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.payements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request->description);
      $request->validate([
        'montant'               => 'required|string|max:60',
        'description'           => 'required|string|max:60',
        'location_id'           => 'required|string|max:60',
      ]);
        // dd($request);

        $payement = Payement::create([
          'date'             =>Carbon::now(),
          'montant'          =>$request->montant,
          'location_id'      =>$request->location_id,
          'description'      =>$request->description,
        ]);

        return redirect()->route('payements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payement  $payement
     * @return \Illuminate\Http\Response
     */
    public function show(Payement $payement)
    {
        return view('pages.payements.show', compact('payement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payement  $payement
     * @return \Illuminate\Http\Response
     */
    public function edit(Payement $payement)
    {
      return view('pages.payements.edit', compact('payement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payement  $payement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payement $payement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payement  $payement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payement $payement)
    {
      $payement->delete();
      return redirect()->route('payements.index');
    }


    public function recu(Payement $payement) {
      
      $pdf = PDF::loadView('pages.payements.recupdf', compact('payement'));

      // download PDF file with download method
      return $pdf->download('pdf_file.pdf');
    }
}
