<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Logement;
use App\Models\Location;
use App\Models\TypeLogement;
use App\Models\Contrat;
use Illuminate\Http\Request;
use PDF;
use Image;
use DateTime;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('pages.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = TypeLogement::all();
        $logements = Logement::all();
        return view('pages.clients.create', compact('logements','types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $fdate = $request->date_debut;
      $tdate = $request->date_fin;
      $datetime1 = new DateTime($fdate);
      $datetime2 = new DateTime($tdate);
      $interval = $datetime1->diff($datetime2);
      // dd($interval);
      $photo = $request->file('piece_identite');
        $request->validate([
          'nom'                 => 'required|string|max:60',
          'prenom'              => 'required|string|max:60',
          'adresse'             => 'required|string|max:60',
          'telephone'           => 'required|string|max:60',
        ]);
        $client = Client::create([
          'nom'                    => $request->nom,
          'prenom'                 => $request->prenom,
          'adresse'                => $request->adresse,
          'sexe'                   => $request->sexe,
          'telephone'              => $request->telephone,
          'age'                    => $request->age,
          'piece_identite'         => $request->piece_identite,
          'profession'             => $request->profession,
          'email'                  => $request->email,
          'personne_a_contacter'   => $request->personne_a_contacter,
          'numero_a_contacter'     => $request->numero_a_contacter,
        ]);

        if ($request->file('piece_identite')->isValid()) {
            $avatar = $request->file('piece_identite');
            $filename = time() . '-piece-identite-.' . $request->piece_identite->extension();
            $filename = '/uploads/avatars/' . $filename;
            // dd($filename);
            $location = public_path($filename);
            Image::make($avatar)->save($location);
            $client->piece_identite = $filename;
            $client->save();
        }

        // $img = Image::make($photo)->resize(320, 240)->insert('public/watermark.png');

        $location = Location::create([
          'date_debut'        =>$request->date_debut,
          'date_fin'          =>$request->date_fin,
          'client_id'         =>$client->id,
          'logement_id'       =>$request->logement,
        ]);
        $loge = Logement::where('id', $location->logement->id);
        $loge->update([
          'status'   =>"occupé",
        ]);
        $contrat = Contrat::create([
          'duree'             =>$interval->format('%m mois %d jours'),
          'date'              =>now(),
          'location_id'       =>$location->id,
        ]);


        return redirect()->route('clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
      // dd($client->piece_identite);
      $locations = Location::where('client_id', $client->id)->latest();
      // dd($locations);
        return view('pages.clients.show', compact('client', 'locations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('pages.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return redirect()->route('clients.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createLocation(Request $request, $id)
    {
        $client = Client::find($id);
        return view('pages.clients.location', compact('client'));
    }


}
