<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $fillable = [
      'date_fin',
      'date_debut',
      'client_id',
      'logement_id',
    ];

    public function client()
    {
      return $this->belongsTo('App\Models\Client', 'client_id');
    }
    public function logement()
    {
      return $this->belongsTo('App\Models\Logement', 'logement_id');
    }
}
