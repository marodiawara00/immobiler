<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payement extends Model
{
    use HasFactory;

    protected $fillable = [
      'date',
      'montant',
      'location_id',
    ];

    public function location($value='')
    {
      return $this->belongsTo('App\Models\Location', 'location_id');
    }
}
