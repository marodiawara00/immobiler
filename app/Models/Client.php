<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
      'nom',
      'prenom',
      'email',
      'telephone',
      'adresse',
      'age',
      'sexe',
      'personne_a_contacter',
      'numero_a_contacter',
      'piece_identite',
      'profession',
    ];

    public function locations($value='')
    {
      return $this->hasMany('App\Models\Location');
    }
}
