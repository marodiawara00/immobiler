<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logement extends Model
{
    use HasFactory;

    protected $fillable = [
      'adresse',
      'commune',
      'quartier',
      'code',
      'prix',
      'status',
      'description',
      'type',
      'pictures',
    ];

    public function type()
    {
      return $this->belongsTo('App\Models\TypeLogement', 'type');
    }
}
