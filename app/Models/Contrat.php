<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contrat extends Model
{
    use HasFactory;

    protected $fillable = [
      'date',
      'duree',
      'location_id',
    ];

    public function location()
    {
      return $this->belongsTo('App\Models\Location', 'location_id');
    }
}
